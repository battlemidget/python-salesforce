python-salesforce

[![Build Status](https://travis-ci.org/debugmonkey/python-salesforce.png?branch=develop)](https://travis-ci.org/debugmonkey/python-salesforce)

Python library and client for authorization and authentication against Salesforce RESTful interface

Supports:

  * OAuth 1.0a
  * Salesforce API v27.0
  * Salesforce JSON implementation
  * Python 2.6, 2.7, and pypy.

Examples:

  * Check in scripts/ directory for authorization and api calling

Copyright 2013 Adam Stokes <adam.stokes@ubuntu.com>
MIT
